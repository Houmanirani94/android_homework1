package edu.sjsu.android.mortgage_calculater;

public class LoanUtil {


    public static double calculate(double P, double I, double Y, boolean isTaxesInsurance) {


        double J = I / 1200;
        double N = Y * 12;
        double T = (isTaxesInsurance) ? P * 0.1 / N : 0.0;

        if(I == 0.0) return isZero( P,  N,  T);
        else return notZero( P,  J,  N,  T);
    }

    private static double isZero(double P, double N, double T) {
        return P / N + T;
    }

    private static double notZero(double P, double J, double N, double T) {
        return (P * (J / (1 - Math.pow((1 + J), -N))) + T);
    }

}
