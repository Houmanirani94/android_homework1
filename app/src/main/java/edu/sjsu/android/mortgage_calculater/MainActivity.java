package edu.sjsu.android.mortgage_calculater;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    private double interestRate;
    private EditText AmountBorrowed;
    private TextView IRTextView;
    private SeekBar IRSeekBar;
    private RadioGroup LoanTerm;
    private CheckBox TaxesInsurance;
    private TextView MonthlyPayment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AmountBorrowed = (EditText) findViewById(R.id.AmountBorrowed);
        IRTextView = (TextView) findViewById(R.id.IRTextView);
        IRSeekBar = (SeekBar) findViewById(R.id.IRSeekBar);
        LoanTerm = (RadioGroup) findViewById(R.id.LoanTerm);
        TaxesInsurance = (CheckBox) findViewById(R.id.TaxesInsurance);
        MonthlyPayment = (TextView) findViewById(R.id.MonthlyPayment);


        IRSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    interestRate = IRSeekBar.getProgress() / 10.0;
                    IRTextView.setText(interestRate + " % ");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void onClick(View view) {

        String input = AmountBorrowed.getText().toString();


        if(input.length() == 0) {
            Toast.makeText(this, "Amount borrowed cannot be empty!", Toast.LENGTH_LONG).show();
            return;
        } else if(!input.matches("\\d+(\\.\\d+)?")) {
            Toast.makeText(this, "Invalid number!", Toast.LENGTH_LONG).show();
            return;
        }

        double P = Double.parseDouble(AmountBorrowed.getText().toString());

        double I = IRSeekBar.getProgress() / 10.0;

        double Y;
        if (((RadioButton) findViewById(R.id.fifteen)).isChecked())
            Y = 15;
        else if (((RadioButton) findViewById(R.id.twenty)).isChecked())
            Y = 20;
        else if (((RadioButton) findViewById(R.id.thirty)).isChecked())
            Y = 30;
        else {
            Toast.makeText(this, "Select a loan term!", Toast.LENGTH_LONG).show();
            return;
        }
        boolean isTaxesInsurance = TaxesInsurance.isChecked();

        double M = LoanUtil.calculate(P, I, Y, isTaxesInsurance);
        String output = String.format("$%.2f", M);
        MonthlyPayment.setText("Monthly Payment: " + output);
    }

}

